$(document).ready(function() {
	var a = document.getElementById("f16Img");
	var b = document.getElementById("f9Img");

	a.style.display = "none";
	b.style.display = "none";
	
	$('#f16Span').click(function() {	
		if (a.style.display == "none") {
        	a.style.display = "inline-block";
    	} else {
        	a.style.display = "none";
    	}
	});

	$('#f9Span').click(function() {	
		if (b.style.display == "none") {
        	b.style.display = "inline-block";
    	} else {
        	b.style.display = "none";
    	}
	});

	$('#workButton').click(function() {	
		if($('.workHistory').is(':visible')){
			$('.workHistory').hide(1000);
		}else{
			$('.schoolHistory').hide(1000);
			$('.workHistory').show(1000);
		}
	});

	$('#educationButton').click(function() {	
		if($('.schoolHistory').is(':visible')){
			$('.schoolHistory').hide(1000);
		}else{
			$('.workHistory').hide(1000);
			$('.schoolHistory').show(1000);
		}
	});
	
	window.onscroll = function() {stickyFunc()};
	var c = document.getElementById("resumeNav");
	var sticky = resumeNav.offsetTop;

	function stickyFunc() {
  		if (window.pageYOffset >= sticky) {
    		c.classList.add("sticky");
  		} else {
    		c.classList.remove("sticky");
  		}
	}

	


});