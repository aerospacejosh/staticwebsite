$(document).ready(function(){
	var a = document.getElementById("addAcronym");
	var b = document.getElementById("addDefinition");

	$('#results').empty(); //clear out initial placeholder html
	document.getElementById("results").style.display = "block";
	clearHideAddStuff();
	//end initialization stuff


	$('#search').keyup(function(){ //execute every time search field changes
		var searchValue = ($('#search').val()).trim(); //get rid of leading/trailing whitespace
		clearHideAddStuff();
		search(searchValue);
	});

	$('#addAcronym').keyup(function(){ //execute every time fields change
		searchAddValue();
	});

	$('#addDefinition').keyup(function(){ //execute every time fields change
		searchAddValue();
	});

	$("#addButton").click(function() {
		var aInput = ($('#addAcronym').val()).trim();
		var bInput = ($('#addDefinition').val()).trim();
		
		if ((a.style.display == "none") || (b.style.display == "none")) { //hidden? unhide
			showAddStuff();
		} else if (aInput == '' && bInput == '') { //unhidden and empty? hide
			clearHideAddStuff();
		} else if (aInput == '' || bInput == '') { //missing one of inputs? Prompt
			alert("You are missing an input!");
		} else { // both inputs full, assume ready to add
			addFunction(aInput, bInput);
		}
	});

	function search(searchInput) { //returns true if anything found, else false
		if(searchInput != '') {
			$.getJSON('acronyms.json', function(result) {
				var list = result;
				//var reMadeJSON = JSON.stringify(list, null, 2);
				//console.log(reMadeJSON);
				var options = {
					shouldSort: true,
					threshold: 0.6,
					location: 0,
					distance: 100,
					maxPatternLength: 32,
					minMatchCharLength: 1,
					keys: [
						"acronym",
						"definition"
					]
				};
				var fuse = new Fuse(list, options);
				var searchResult = fuse.search(searchInput);

				if(searchResult.length > 0) {
					$('#results').empty();
					for(i=0; i < searchResult.length; i++) {
						$('#results').append('<div class="res"><span>'+searchResult[i].item.acronym+'</span><span>'+':  '+searchResult[i].item.definition+'</span></div>');
					}
				} else {
					$('#results').empty();
				}
			})
		} else {
			$('#results').empty();
		}
	}

	function searchAddValue() {
		var comboValue = ($('#addAcronym').val()).trim() + ' ' + ($('#addDefinition').val()).trim(); //get rid of leading/trailing whitespace
		$('#search').val('');
		search(comboValue);
	}

	function addFunction(x, y) {
		var password = prompt('Enter password to add ' + x + ': ' + y);
		if (password == 'joshp') {
			console.log('Adding ' + x + ': ' + y);
			//add actually saving here
			clearHideAddStuff();
			$('#search').val(x + ' ' + y);
			search(x + ' ' + y); //no keyup to trigger search from above
		} else if (password != '') {
			console.log('Incorrect Password - self destruct initiated');
		}
	}

	function clearHideAddStuff() {
		$('#addAcronym').val('');
		$('#addDefinition').val('');
		a.style.display = "none";
		b.style.display = "none";
	}

	function showAddStuff() {
		a.style.display = "inline";
		b.style.display = "inline";
	}
})