$(document).ready(function() {
	var a = document.getElementById("marsAttacksImg");
	var b = document.getElementById("gremlinImg");
	var c = document.getElementById("abductionImg");
	var d = document.getElementById("scubaImg");
	var e = document.getElementById("dive2Img");
	var f = document.getElementById("triggerImg");
	var counter = 11;

	function hide(){
		a.style.display = "none";
		b.style.display = "none";
		c.style.display = "none";
		d.style.display = "none";
		e.style.display = "none";
	}
	
	hide();

	function everyClick(){
		$("h2").text("Thank you for sampling some art!");
		counter--;
		$('#counter').text(counter);

		if (counter==0){
			$("h1").text("Trigger");
			$("h2").text("My that is a lot of art you looked at!");
			$(f).slideDown(2000);
		}

		if (counter==-1){
			f.style.display = "none";
			$("h1").text("Art");
			$("h2").text("Thank you for sampling some art!");
		}
	}
	
	$('#marsButton').click(function() {
		everyClick();
		if (a.style.display == "none" && counter!=0) {
        	hide();
        	$(a).slideDown(1000);
    	} else {
        	hide();
    	}
	});

	$('#gremlinButton').click(function() {	
		everyClick();
		if (b.style.display == "none" && counter!=0) {
        	hide();
        	$(b).slideDown(1000);
    	} else {
        	hide();
    	}
	});

	$('#abductionButton').click(function() {	
		everyClick();
		if (c.style.display == "none" && counter!=0) {
        	hide();
        	$(c).slideDown(1000);
    	} else {
        	hide();
    	}
	});

	$('#scubaButton').click(function() {	
		everyClick();
		if (d.style.display == "none" && counter!=0) {
        	hide();
        	$(d).slideDown(1000);
    	} else {
        	hide();
    	}
	});

	$('#dive2Button').click(function() {	
		everyClick();
		if (e.style.display == "none" && counter!=0) {
        	hide();
        	$(e).slideDown(1000);
    	} else {
        	hide();
    	}
	});

});