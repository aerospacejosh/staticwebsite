AFRAME.registerComponent('caminsideandbackhome', {
 
   init: function () {
      
      let goInsideBackHomeFunc = () => {
        showHome(true);
        setSky("", "#00AAFF")
        camMove(0, 1.6, -14, 0);
      }

      this.el.addEventListener('click', goInsideBackHomeFunc);
        
   }});