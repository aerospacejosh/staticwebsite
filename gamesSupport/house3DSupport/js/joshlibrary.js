let enableLoadPano = true; //used to prevent hidden pano spheres from being clicked when in another pano

//moves camera to location and uses camera parent rig to force rotation
let camMove = (posX, posY, posZ, rotRadY) => {
    
  let camPosRig = document.querySelector("#camPosRig");
  let camRotRig = document.querySelector("#camRotRig");
  let cam = document.querySelector("#camera");

  cam.object3D.position.set(0, 0, 0); //grandchild
  camPosRig.object3D.position.set(posX, posY, posZ); //parent
  //camRotRig.object3D.rotation.set(0, 0, 0); //child
      
  //how much additional rotation is needed to match goal given can't force rotate camera directly
  let offsetNeededRadians = (cam.object3D.rotation.y * -1) + rotRadY;
      
  camRotRig.object3D.rotation.set(0, offsetNeededRadians, 0);
}

//show or hide home area objects
let showHome = (showBool) => {
    
  let homeworldelements = document.querySelectorAll(".homeArea");
  
  homeworldelements.forEach((homeworldelement) => {
    homeworldelement.setAttribute("visible", showBool)
  })
  
  enableLoadPano = showBool; //allow loading panos if home, but prevent if in another pano
}

//set sky
let setSky = (skyString, skyColor) => {
    
  let sky = document.querySelector("#sky");
  sky.setAttribute("src", skyString);
  sky.setAttribute("color", skyColor);
 
}

let faceExpand = true;
//set sky
let facezillaconfigure = () => {
    
  let face = document.querySelector("#face");
  if (faceExpand == true) {
    face.object3D.scale.set(1, 1, 1);
    camMove(0, 1.6, 0, 0);
    faceExpand = false;
  } else {
    face.object3D.scale.set(.003, .003, .003);
    faceExpand = true;
  }
}