AFRAME.registerComponent('kinlock21comp', {
    
   init: function () {

      let loadPanoKinlock21Func = () => {
        if (enableLoadPano == true) {
          setSky("#panoKinlock21", "#FFFFFF");
          showHome(false);
        }
      }

      this.el.addEventListener('click', loadPanoKinlock21Func);
        
   }});