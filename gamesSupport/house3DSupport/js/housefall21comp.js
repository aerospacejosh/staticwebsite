AFRAME.registerComponent('housefall21comp', {
    
   init: function () {

      let loadHomePanoFunc = () => {
        if (enableLoadPano == true) {
          setSky("#panoHouseFall21", "#FFFFFF");
          showHome(false);
        }
      }

      this.el.addEventListener('click', loadHomePanoFunc);
        
   }});