AFRAME.registerComponent('houstonhouse21comp', {
    
   init: function () {

      let loadPanoHoustonHouse21Func = () => {
        if (enableLoadPano == true) {
          setSky("#panoHoustonHouse21", "#FFFFFF");
          showHome(false);
        }
      }

      this.el.addEventListener('click', loadPanoHoustonHouse21Func);
        
   }});