AFRAME.registerComponent('highfallswintercomp', {
    
   init: function () {

      let loadPanoHighFallsWinterFunc = () => {
        if (enableLoadPano == true) {
          setSky("#panoHighFallsWinter", "#FFFFFF");
          showHome(false);
        }
      }

      this.el.addEventListener('click', loadPanoHighFallsWinterFunc);
        
   }});