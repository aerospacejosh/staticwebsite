// Classic pong game with a new twist: 3 powerups, some things in color, and horribly-awesome sound effects
// Some basic code was started from Chris Deleon's great course @ https://www.udemy.com/code-your-first-game/

var canvas;
var canvasContext;

// sounds
var soundToggle = 1;
var soundAlright;
var soundBounce; // only sound that could play overlapping itself
var soundBounceArray = []; // will create multiple instances in memory
var soundBounceArrayIndex = 0;
var soundDangIt;
var soundKapow;
var soundSweet;
var soundWompWomp;

// ball speed and velocity 2d
var ballX;
var ballSpeedX;
var ballY;
var ballSpeedY;

// Mods
var modPowerUpX;
var modPowerUpY;
var modPowerUpType; // 1=Tracer, 2=Predict hit pointers, 3=Computer paddle slowdown
var modPowerUpColor;
var modPowerUpExplodeSize; // radius changes when hit
var modPowerUpExplodeTimer;
var modPowerUpDistance;

var modTracer = false;
var ballTracerX = [];
var ballTracerY = [];

var modPredict = false;
var ballPredictX = ballX;
var ballPredictY = ballY;

var modCompSlowDown = false;
var compMaxSpeed; // max allowed computer movement pixels per frame

var paddle1Y = 250;
var paddle2Y = 250;
var paddle2YSpeed = 0; // computer paddle's velocity
const PADDLE_HEIGHT = 100;
const PADDLE_WIDTH = 10;

// conditionalExtras() toggles
// start with game paused and title screen shown
var pauseGame = true;
var showTitleScreen = true;
var showAddPoint = false;
var showWinScreen = false;
var timer = 0;
var swapColor = 1;
var colorHexValue = 0; // for use with #RRGGBB hex mods
var clicked = 0; // used to ensure click needed to pass title screen
// ...used because must click something to enable sounds per HTML5 rules

// Title screen
var numXBlocks = 21;
var numYBlocks = 8;
var blockWidth;
var blockHeight;
var xOffset;
var xMove;
// Spell out pong in grid blocks
// each subarray is a row and each index represents a column's block to fill in
var grid = [[1,2,3,4,6,7,8,9,11,14,16,17,18,19],
			[1,4,6,9,11,12,14,16],
			[1,2,3,4,6,9,11,12,14,16,18,19],
			[1,6,9,11,13,14,16,19],
			[1,6,7,8,9,11,13,14,16,17,18,19]];

// JP symbol background
var grid2 = [[10,11,12,13],
			 [10,13],
			 [10,11,12,13],
			 [7,10],
			 [7,10],
			 [7,8,9,10]];

// scoring stuff
const MATCH_POINTS = 3;
var humanScore = 0;
var computerScore = 0;
var lastScored; // 0=human, 1=AI
var yTextWinPos = -30;

// button uses this to toggle sound
function soundToggleFunc() {
	soundToggle *= -1;
}

function calculateMousePos(evt) {
	let rect = canvas.getBoundingClientRect();
	var root = document.documentElement;
	var mouseX = evt.clientX - rect.left - root.scrollLeft;
	var mouseY = evt.clientY - rect.top - root.scrollTop;
	return {
		x: mouseX,
		y: mouseY
	};
}

function ballReset() {
	modPowerUpCreate(); 
	ballX = canvas.width / 2;
	// set new ball location at player 2's paddle height
	// ensure don't put ball outside of 50pixels of edge top or bottom
	ballY = Math.min(canvas.height-50,Math.max(50,paddle2Y + PADDLE_HEIGHT / 2));
	// go towards player 2 horizontally with random small y values
	ballSpeedX = 10;
	ballSpeedY = genRandInt(-5, 5);
	predictHit(); // figures out where ball will hit next (helps AI)
}

// generate a powerup and place on canvas
// intention if ball hits, powerup becomes active
function modPowerUpCreate() {
	modTracer = false;
	ballTracerX = [];
	ballTracerY = [];
	modPredict = false;
	modCompSlowDown = false;
	modPowerUpExplodeSize = 0;
	modPowerUpExplodeTimer = 0;

	modPowerUpX = genRandInt(100, canvas.width-100);
	modPowerUpY = genRandInt(100, canvas.height-100);
	modPowerUpType = genRandInt(1,3);

	if(modPowerUpType == 1) modPowerUpColor = 'grey';
	if(modPowerUpType == 2) modPowerUpColor = 'blue';
	if(modPowerUpType == 3) modPowerUpColor = 'green';
}

// contains main game loop
window.onload = function() {
	canvas = document.getElementById('gameCanvas');
	canvasContext = canvas.getContext('2d');

	// sounds
	soundAlright = document.getElementById("alright");
	soundBounce = document.getElementById("bounce");
	for(let i = 0; i < 3; i++) {
		// create 3 new instances of bounce.mp3 in memory for overlapping playback(4 counting original)
		soundBounceArray.push(soundBounce.cloneNode())
		}
	soundDangIt = document.getElementById("dangIt");
	soundKapow = document.getElementById("kapow");
	soundSweet = document.getElementById("sweet");
	soundWompWomp = document.getElementById("wompWomp");

	// set variables for title screen
	blockWidth = canvas.width / numXBlocks;
	blockHeight = canvas.height / numYBlocks;
	xOffset = -canvas.width;
	xMove = canvas.width/15;

	ballReset();//start ball off randomly

	// set game loop
	var framesPerSecond = 30;
	setInterval(function() {
		conditionalExtras();
		moveEverything();
		drawEverything();
	}, 1000/framesPerSecond);

	// used for human paddle movement WITH MOUSE
	canvas.addEventListener ('mousemove',
		function (evt) {
			var mousePos = calculateMousePos(evt);
			paddle1Y = mousePos.y - (PADDLE_HEIGHT / 2); // center paddle on mouse
		});

	// used for human paddle movement WITH TOUCHSCREEN
	canvas.addEventListener ('touchmove',
		function (evt) {
			evt.preventDefault(); // disable other things touch does like scrolling
			let rect = canvas.getBoundingClientRect(); // returns object with coordinates of top left of canvas
			paddle1Y = evt.touches[0].screenY - (PADDLE_HEIGHT / 2) - rect.top; // center paddle on touch
		});

	// used for initial continue past title screen to ensure sound allowed to play
	// (must interact with page in some way to allow any sound per HTML 5 rules)
	canvas.addEventListener ('click',
		function() {
			clicked = 1;
		});
}

function genRandInt(min, max){
	let randNumFloat = Math.random() * (max - min) + min;
	return Math.round(randNumFloat);
}

// as soon as ball hits left or right paddle, used to predict where it will hit opposite edge
function predictHit(){
	ballPredictX = ballX;
	ballPredictY = ballY;
	let ballPredictSpeedY = ballSpeedY; // only use in this function


	while(ballPredictX < canvas.width && ballPredictX > 0) { // while haven't hit left or right edge...
		ballPredictX += ballSpeedX;
		ballPredictY += ballPredictSpeedY;
		if(ballPredictY > canvas.height || ballPredictY < 0) ballPredictSpeedY *= -1; // if hit top or bottom, deflect y
	}
}

function computerMovement() {
	if(modCompSlowDown) {
		compMaxSpeed = 4; // slow down to max 4 pixels/frame
	} else {
		compMaxSpeed = 7; // normal speed if mod not enabled
	}
	// regular movement stuff
	// actual paddle target will be average between ball y position and exact point it will hit
	let targetPosMatchY = ballY - (PADDLE_HEIGHT / 2);
	let targetPosExactY = ballPredictY - (PADDLE_HEIGHT / 2);
	let targetPos = (targetPosMatchY + targetPosExactY) / 2;
	// accelerate slowly in right direction outside of deadband and within accel limit
	if(paddle2Y + 25 < targetPos && paddle2YSpeed <= compMaxSpeed) paddle2YSpeed++;
	if(paddle2Y - 25 > targetPos && paddle2YSpeed >= -compMaxSpeed) paddle2YSpeed--;
	// if after 5 moves will overshoot, start accelerating in opposite direction
	if(paddle2Y < targetPos && paddle2YSpeed >= -compMaxSpeed && paddle2Y + paddle2YSpeed * 5 > targetPos) paddle2YSpeed-=2;
	if(paddle2Y > targetPos && paddle2YSpeed <= compMaxSpeed && paddle2Y + paddle2YSpeed * 5 < targetPos) paddle2YSpeed+=2;
	// set new paddle location within reasonable bounds
	paddle2Y = Math.min(canvas.height - PADDLE_HEIGHT / 2, Math.max(-PADDLE_HEIGHT / 2, paddle2Y + paddle2YSpeed));
}

function askUserToClick() {
	if(colorHexValue == 10 || colorHexValue == 99) swapColor *= -1; // swap color alternates between 1 and -1 to hold colorHexValue within bounds
	colorHexValue = (colorHexValue + swapColor) % 90 + 10; // increments 10-99 and back to 10, then repeats
	// concatenate strings to make an RGB color string '#RRGGBB'
	drawText('<< CLICK TO START >>', 40, '#' + colorHexValue.toString() + colorHexValue.toString() + colorHexValue.toString(), canvas.width / 2 - 225, canvas.height - 60);
}

// called >=1 times every time power up is hit by ball
function modPowerUpHit() {
	if(modPowerUpType == 1) modTracer = true;
	if(modPowerUpType == 2) modPredict = true;
	if(modPowerUpType == 3) modCompSlowDown = true;

	if(modPowerUpExplodeTimer < 1) { // start explosion (rest carried out in drawEverything()
		modPowerUpExplodeTimer++;
		if(soundToggle == 1) {
			soundKapow.play();
		}
	} 
}

function conditionalExtras() {
	if(showTitleScreen) {
		colorRect(0, 0, canvas.width, canvas.height, 'black'); // background
		// Pong intro
		
		if(timer < 15) { // progressively move coordinates for slide in
			xOffset = Math.floor(xOffset + xMove);
			timer++;
		} else if(clicked == 0) { // ask user to click screen for first game only
			askUserToClick();
			timer = 45; // ensure no wait time after user clicks to continue
		} else if(clicked == 1) {
			timer++; // ensures minimum wait with "Pong" on screen before slide out
		}
		if(timer > 45) { // move coordinates again for slide out after min wait and click
			xOffset = Math.floor(xOffset + xMove);
			timer++;
		}

		// Draw letters from grid and 2d array
		for(let row = 0; row<grid.length; row++) {
			for(let column = 0; column<grid[row].length; column++) {
				colorRect(Math.floor(blockWidth*grid[row][column]+xOffset), Math.floor(blockHeight * (row + 1)), Math.floor(blockWidth), Math.floor(blockHeight-1), 'white');
			}// end inner for
		}// end outer for

		if(timer >= 80) {
			showTitleScreen = false;
			timer = 0;
			xOffset = -canvas.width; // reset
			pauseGame = false;
		}
	} else if(showAddPoint) {
		// draw small background rect and text since without rect text leaves trail
		if(lastScored == 0) { // human scored
			colorRect(100,0,50,80, 'black');
			drawText('+1', 40, 'green', 100, yTextWinPos);
		} else { // AI scored
			colorRect(canvas.width-100,0,50,80, 'black');
			drawText('+1', 40, 'red', canvas.width-100, yTextWinPos);
		}

		// animate text dropping down
		if(timer<12) yTextWinPos+=7;
		timer++;

		// after one second, finish up
		if(timer>=30) {
			showAddPoint = false;
			timer = 0;
			yTextWinPos = -30;
			pauseGame = false;
			// enough points to win or lose overall?
			if(computerScore >= MATCH_POINTS || humanScore >= MATCH_POINTS) {
				computerScore = 0;
				humanScore = 0;
				showWinScreen = true;
				pauseGame = true;
			}
		}
	} else if(showWinScreen) {
		if(timer % 5 ==0) swapColor *= -1; // toggle -1 to 1 at regular interval

		if(lastScored == 0) {
			if(soundToggle == 1 && timer == 0) {
					soundAlright.play();
				}
			if(swapColor < 0) {
				drawText('WIN!', 350, 'green', 20, 420);
			} else {
				drawText('WIN!', 350, 'white', 20, 420);
			}
		} else {
			if(soundToggle == 1 && timer == 0) {
					soundWompWomp.play();
				}
			if(swapColor < 0) {
				drawText('GAME', 200, 'red', 100, 250);
				drawText('OVER', 200, 'red', 100, 450);
			} else {
				drawText('GAME', 200, 'white', 100, 250);
				drawText('OVER', 200, 'white', 100, 450);
			}
		}

		timer++;
		if(timer>=60) {
			showWinScreen = false;
			timer = 0;
			showTitleScreen = true;
		}
	}
}

function moveEverything() {
	if(pauseGame) {
		return; // do nothing if conditionalExtras() going
	} else {
		computerMovement();

		// Move ball location per frame based on 2d speed
		ballX += ballSpeedX;
		ballY += ballSpeedY;

		// Hit power up?
		modPowerUpDistance = Math.sqrt(Math.pow(modPowerUpX - ballX,2) + Math.pow(modPowerUpY - ballY,2)); // dist between ball and power up
		if(modPowerUpDistance < 32) {
			modPowerUpHit();
		}

		// Tracer ball stuff
		if(modTracer) {
			ballTracerX.unshift(ballX); // adds to beginning of array and shifts rest over
			ballTracerY.unshift(ballY);
			if(ballTracerX.length > 30) {
				ballTracerX.pop(); // removes last from array
				ballTracerY.pop();
			}
		}

		// Hit left edge?
		if(ballX < 0) {
			if(ballY > paddle1Y && ballY < paddle1Y + PADDLE_HEIGHT) { // Hit human paddle?
				if(soundToggle == 1) {
					soundBounceArray[soundBounceArrayIndex].play();
					soundBounceArrayIndex = (soundBounceArrayIndex + 1) % 3 // iterate 0-2 repeatedly
				}
				ballSpeedX *= -1;
				ballX = 1; //move ball back onscreen
				// calc varying y speed based on where hit paddle
				var deltaPaddle1Middle = ballY - (paddle1Y + (PADDLE_HEIGHT / 2))
				ballSpeedY = deltaPaddle1Middle / 3;
				predictHit();
			}else { // human missed
				if(soundToggle == 1) {
					soundDangIt.play();
				}
				computerScore++;
				lastScored = 1; // so showAddPoint knows who scored last
				ballReset();
				pauseGame = true;
				showAddPoint = true;
			}	
		}

		// Hit right edge?
		if(ballX > canvas.width) {
			if(ballY > paddle2Y && ballY < paddle2Y + PADDLE_HEIGHT) { // Hit computer paddle?
				if(soundToggle == 1) {
					soundBounceArray[soundBounceArrayIndex].play();
					soundBounceArrayIndex = (soundBounceArrayIndex + 1) % 3 // iterate 0-2 repeatedly
				}
				ballSpeedX *= -1;
				ballX = canvas.width - 1; //move ball back onscreen
				// calc varying y speed based on where hit paddle
				var deltaPaddle2Middle = ballY - (paddle2Y + (PADDLE_HEIGHT / 2))
				ballSpeedY = deltaPaddle2Middle / 3;
				predictHit();
			}else { // computer missed
				if(soundToggle == 1) {
					soundSweet.play();
				}
				humanScore++;
				lastScored = 0;
				ballReset();
				pauseGame = true;
				showAddPoint = true;
			}	
		}

		// Hit top and still trying to go upwards --OR-- hit bottom and still trying to go down?
		if((ballY < 0 && ballSpeedY < 0) || (ballY > canvas.height && ballSpeedY > 0)) {
			ballSpeedY *= -1;
			if(soundToggle == 1) {
					soundBounceArray[soundBounceArrayIndex].play();
					soundBounceArrayIndex = (soundBounceArrayIndex + 1) % 3 // iterate 0-2 repeatedly
				}
		}
	}
}

function drawEverything() {
	if(pauseGame) {
		return; // do nothing if conditionalExtras() is going
	} else {
		// Normal in-game drawing
		colorRect(0, 0, canvas.width, canvas.height, 'black'); // background
		
		// JP symbol
		for(let row = 0; row<grid2.length; row++) {
			for(let column = 0; column<grid2[row].length; column++) {
				colorRect(Math.floor(blockWidth*grid2[row][column]), Math.floor(blockHeight * (row + 1)), Math.floor(blockWidth), Math.floor(blockHeight), '#111111');
			}// end inner for
		}// end outer for
		
		// Powerup, if enabled, leaves a one second trail of grey balls
		if(modTracer) {
			for(let i = 0; i < ballTracerX.length; i++) {
				colorCircle(ballTracerX[i], ballTracerY[i], 10, 'grey');
			}
		}

		// Draw power up if not hit and not exploded yet
		if(modPowerUpExplodeTimer < 30) drawStar(modPowerUpX, modPowerUpY, modPowerUpColor);
		// Animate explosion growing if power up hit
		if(modPowerUpExplodeTimer > 0 && modPowerUpExplodeTimer < 30) {
			colorCircle(modPowerUpX, modPowerUpY, ++modPowerUpExplodeSize, 'red');
			modPowerUpExplodeTimer++;
		}

		// Draw computer paddle
		if(modCompSlowDown) {
			colorRect(canvas.width - PADDLE_WIDTH, paddle2Y, PADDLE_WIDTH, PADDLE_HEIGHT, 'grey'); // show computer paddle slower by different color
		} else {
			colorRect(canvas.width - PADDLE_WIDTH, paddle2Y, PADDLE_WIDTH, PADDLE_HEIGHT, 'white'); // paddle 2
		}
		
		// Human paddle
		colorRect(0, paddle1Y, PADDLE_WIDTH, PADDLE_HEIGHT, 'white');

		// Ball
		colorCircle(ballX, ballY, 10, 'white'); // ball

		// Scoring
		drawText(humanScore, 20, 'white', 100, 100);
		drawText(computerScore, 20, 'white', canvas.width-100, 100);
		// Next hit prediction marks
		if(modPredict) {
			if(ballPredictX > 0) {
				// draw a triangle pointer on right side screen
				canvasContext.fillStyle = 'blue';
				canvasContext.beginPath();
				canvasContext.lineTo(canvas.width-35, ballPredictY-15);
				canvasContext.lineTo(canvas.width-35, ballPredictY+15);
				canvasContext.lineTo(canvas.width, ballPredictY);
				canvasContext.closePath();
				canvasContext.fill();
			} else {
				// draw triangle pointer on left side screen
				canvasContext.fillStyle = 'blue';
				canvasContext.beginPath();
				canvasContext.lineTo(35, ballPredictY-15);
				canvasContext.lineTo(35, ballPredictY+15);
				canvasContext.lineTo(0, ballPredictY);
				canvasContext.closePath();
				canvasContext.fill();
			}
		}
	}
}

function drawStar(centerX, centerY, color) {
	canvasContext.fillStyle = color;
	canvasContext.beginPath();
	canvasContext.lineTo(centerX, centerY-22); // top point
	canvasContext.lineTo(centerX-12, centerY+20); // bottom left
	canvasContext.lineTo(centerX+20, centerY-4); // right
	canvasContext.lineTo(centerX-20, centerY-4); // left
	canvasContext.lineTo(centerX+12, centerY+20); // bottom right
	canvasContext.closePath();
	canvasContext.fill();
}

function drawText(text, size, color, leftX, topY) {
	let fontCombo = size.toString() + 'px sans-serif';
	canvasContext.font = fontCombo;
	canvasContext.fillStyle = color;
	canvasContext.fillText(text, leftX, topY);
}

function colorCircle(centerX, centerY, radius, drawColor) {
	canvasContext.fillStyle = drawColor;
	canvasContext.beginPath();
	//Draw including start and stop radian positions and clockwise
	canvasContext.arc(centerX, centerY, radius, 0, Math.PI*2, true);
	canvasContext.fill();

}

function colorRect(leftX, topY, width, height, drawColor) {
	canvasContext.fillStyle = drawColor;
	canvasContext.fillRect(leftX, topY, width, height);
}